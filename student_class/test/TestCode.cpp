#include "gtest/gtest.h"
#include "student.h"
#include "whitelist.h"

TEST(StudentClass, Test_1)
{
    student Student;
    Student.set_age(18);
    Student.set_roll_no("A1");
    ASSERT_EQ(Student.get_roll_no(), "A1");
    ASSERT_EQ(Student.get_age(), 18);
}

TEST(whiteClass, Test_2)
{
    whitelist w_list;
    student s1;
    s1.set_age(18);
    s1.set_roll_no("A1");
    w_list.add_to_whitelist("A", s1);
    ASSERT_EQ(w_list.is_student_present("A"), true);
    ASSERT_EQ(w_list.get_student_data("B"), nullptr);
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}