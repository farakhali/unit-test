#include <map>
#include <list>

#include "student.h"

class whitelist
{
private:
    std::map<std::string /* Student Name */, student * /* pointer to Student */> _StudentData;
    std::list<student> _StudentDataList; // change contanier from vector -> list
    /* Reasson......
    -> Vector Container is just like Dynamic arrays are arrays whose size is determined while the program is
    running, rather than being fixed when the program is written. when ever its grows/increases its allocate
    new memory because of that reallocation the already stored pointer get NULL value and become useless.

    -> To overcome/avoid this issue we can use list or forward list.
    */
public:
    void add_to_whitelist(std::string student_name, student student_data);
    bool is_student_present(std::string student_name);
    student *get_student_data(std::string student_name);
};
