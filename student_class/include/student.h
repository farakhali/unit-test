#pragma once
#include <iostream>
#include <string>
#include <map>

using namespace std;

class student
{
private:
    struct recode
    {
        string roll_no;
        int age;
        float cgpa;
    } student_recode;
    map<string /*Subject  */, int> result;

public:
    int get_age();
    void set_age(int st_age);

    float get_cgpa();
    void set_cgpa();

    string get_roll_no();
    void set_roll_no(string rollno);

    void set_subj_mark(string subj, int mark);
    int get_subj_mark(string get_mark);


    void print_all_mark();
    
};
