#include <map>
#include <vector>
#include <iostream>

#include "student.h"
#include "whitelist.h"


void whitelist::add_to_whitelist(std::string student_name, student student_data)
{
    _StudentDataList.push_front(student_data);   // insert student record at the front/start of a list
    student *st_ptr = &_StudentDataList.front(); // returns reference to the first element in the list
    _StudentData.insert(std::pair<std::string, student *>(student_name, st_ptr));
}

bool whitelist::is_student_present(std::string student_name)
{
    if (_StudentData.find(student_name) != _StudentData.end()) //  if found return iterator pointing to element otherwise nullptr
    {
        std::cout << "Student Found\t\t";
        return true;
    }
    else
    {
        std::cout << ("Student not found in the map") << "\n";
        return false;
    }
}

student *whitelist::get_student_data(std::string student_name)
{
    auto it = _StudentData.find(student_name);
    if (it != _StudentData.end())
        return it->second;
    else
        return nullptr;
}
