#include "student.h"
float sum = 0;
int count = 0;

int student::get_age()
{
    return student_recode.age;
}
void student::set_age(int st_age)
{
    if (st_age > 17)
    {
        student_recode.age = st_age;
    }
    else
    {
        cout << "Error!! Age must be greater than 17\n";
    }
}
float student::get_cgpa()
{
    return student_recode.cgpa;
}
void student::set_cgpa()
{
    student_recode.cgpa = 0;
    sum = 0;
    count = 0;
    for (map<string, int>::iterator itr = result.begin(); itr != result.end(); ++itr)
    {

        sum = sum + itr->second;
        count++;
    }
    student_recode.cgpa = 4 * (sum / (count * 100));
}
string student::get_roll_no()
{
    return student_recode.roll_no;
}
void student::set_roll_no(string rollno)
{
    student_recode.roll_no = rollno;
}
void student::set_subj_mark(string subj, int mark)
{
    if (mark < 0 || mark > 100)
    {
        cout << "\n"
             << subj << " Marks not set marks must be within 0 and 100.\n";
        return;
    }
    else
    {
        cout << "\n"
             << get_roll_no() << " " << subj << " Subject added";
        result.insert(pair<string, int>(subj, mark));
    }
    set_cgpa();
}
int student::get_subj_mark(string get_mark)
{
    if (result.find(get_mark) != result.end())
    {
        cout << "\n"
             << get_mark << " Subject Marks : ";
        return result[get_mark];
    }
    else
    {
        cout << "\nSubject Not Found ";
        return -1;
    }
}
void student::print_all_mark()
{
    bool flag = true; // bool
    for (map<string, int>::iterator itr = result.begin(); itr != result.end(); ++itr)
    {
        flag = false;
        cout << itr->first << " : " << itr->second << "\t";
    }
    if (flag)
    {
        cout << "No Subject Found\n";
    }
}