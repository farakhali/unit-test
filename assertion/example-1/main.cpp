#include <iostream>
#include <cassert>
using namespace std;

void display_number(int *myInt)
{
    assert(myInt != NULL);
    assert((*myInt) > 0);
    cout << "myInt contains value"
         << " = " << *myInt << endl;
}

int main()
{
    int myptr = 5;
    int *second_ptr = NULL;
    int *third_ptr = NULL;
    second_ptr = &myptr;
    display_number(second_ptr);
    display_number(third_ptr);
    return 0;
}