# unit-test

in this task we mainly focused on the assertion and unit testing using google test in c++.

## How to Run assertion task

To run the assertion task run the following cocmmands

- `git clone https://gitlab.com/farakhali/unit-test.git`
- `cd unit-test/assertion`

To run examples go inside the example directory and follow the instructions

- `cd example-#number`
- `mkdir build`
- `cd build`
- `cmake ..`
- `make`

To run executable run `./main`

## How to Run unit-test task

To run the unit test task run the following cocmmands

- `git clone https://gitlab.com/farakhali/unit-test.git`
- `cd unit-test/student_class`
- `mkdir build`
- `cd build`
- `cmake ..`
- `make`

To run executable run `./test/g-test`

## How to Run unit-test/student_class using Docker Iamge

To run this task using Docker image make sure docker installed on your system
Docker Hub repo link `https://hub.docker.com/repository/docker/farakhali/unit-testing`

### Pull image docker hub repository

how to get image from docker hub and run it  
pull  `docker pulll farakhali/uniit-test:final`  
run it `docker run -it farakhali/unit-test:final`

### Push image to docker hub

Build image from Docker file `docker build ./path/to/file -t name:tag`  
Run docker container `docker run -d -t --name "container name" name:tag`  
Commit Container `docker commit (Container ID) farakhali/unit-test:final`  
login to you docker account `docker login`  
push to docker hub repo `docker push username/name:tag`  
